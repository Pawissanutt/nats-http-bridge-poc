FROM alpine
RUN apk add --update --no-cache python3 && \
    ln -sf python3 /usr/bin/python &&\
    python3 -m ensurepip && \
    mkdir -p "/opt/app"

WORKDIR /opt/app
COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY *.py ./

EXPOSE 8080

ENTRYPOINT ["/bin/sh", "-c"]
#CMD ["python3 main.py"]
CMD ["python3 consumer.py"]

