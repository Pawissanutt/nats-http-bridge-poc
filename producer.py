import asyncio
import datetime
import os
import sys

import nats
from nats.aio.client import Client
from nats.errors import ConnectionClosedError, TimeoutError, NoServersError

NATS_URL = os.getenv('NATS_URL', 'nats://localhost:4222')


async def request(nc: Client,
                  msg: str):
    try:
        response = await nc.request("help", msg.encode(), timeout=30)
        print(f"[{datetime.datetime.now().isoformat()}]Received response: {response.data.decode()}")
    except TimeoutError:
        print("Request timed out")


async def main(msg):
    # It is very likely that the demo server will see traffic from clients other than yours.
    # To avoid this, start your own locally and modify the example to use it.
    nc = await nats.connect(NATS_URL)

    # for i in range(0, 10):
    #     await request(nc, msg)
    await request(nc, msg)

    # Terminate connection to NATS.
    await nc.drain()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        msg = sys.argv[1]
    else:
        msg = "help me"
    asyncio.run(main(msg))
