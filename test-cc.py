import math

from badm import EnergyEstimator, V2DecisionMaker, Battery, V3DecisionMaker, TimeEstimator, RunMode, V4DecisionMaker

# 20 Wh
battery = Battery(current=20, total=20)
energy_estimator = EnergyEstimator()
f_time_estimator = TimeEstimator()
ar_time_estimator = TimeEstimator()
al_time_estimator = TimeEstimator()

# make it live at least 1 hours
# er_threshold = battery.total / 3600
er_threshold = battery.total / 300
print(f"er_threshold: {er_threshold}")
# decision_maker = V2DecisionMaker(battery,
#                                  energy_estimator,
#                                  er_threshold=er_threshold)
decision_maker = V4DecisionMaker(battery=battery,
                                 energy_estimator=energy_estimator,
                                 f_time_estimator=f_time_estimator,
                                 ar_time_estimator=ar_time_estimator,
                                 al_time_estimator=al_time_estimator,
                                 er_threshold=er_threshold)


def simulate_load(num=3000):
    for j in range(0, num):
        d = j ** j


for i in range(0, 20):
    print(f"{i} ================================================== {i}")
    run_mode = decision_maker.decide(deadline=10, log_param=True)
    e_tracker = energy_estimator.tracker()
    print(f"run mode: {run_mode}")
    e_tracker.start()
    if run_mode is RunMode.FAST or run_mode is RunMode.FAST_THEN_ACC:
        t_tracker = f_time_estimator.start()
        # local_net
        simulate_load(3000)
        # off load
        simulate_load(1000)
        t_tracker.stop()
    if run_mode is RunMode.ACC or run_mode is RunMode.FAST_THEN_ACC:
        # local_net
        t_tracker = al_time_estimator.start()
        simulate_load(5000)
        t_tracker.stop()
        # off load
        t_tracker = ar_time_estimator.start()
        simulate_load(1500)
        t_tracker.stop()
    battery.current -= e_tracker.stop()
    energy_estimator.print_state()
print("end")
