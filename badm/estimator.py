from codecarbon import EmissionsTracker

import time
from typing import List


class DataWrapper:
    def __init__(self, ts, val):
        self.ts = ts
        self.val = val

    def __repr__(self):
        return f"({self.val}, {self.ts})"


class SlidingWindow:
    window: List[DataWrapper]

    def __init__(self,
                 window_size=10,
                 max_time_range=30,
                 log_level=0):
        self.window_size = window_size
        self.log_level = log_level
        self.max_time_range = max_time_range
        self.window = []

    def ready(self):
        return len(self.window) > 0

    def add_data(self,
                 val):
        self.window.append(DataWrapper(ts=time.time(), val=val))
        if len(self.window) > self.window_size:
            self.window.pop(0)

    def filter_window(self):
        cut_time = time.time() - self.max_time_range
        filtered_window = [x for x in self.window if x.ts > cut_time]
        if len(filtered_window) == 0 and self.ready():
            return [self.window[-1]]
        else:
            return filtered_window

    def compute_avg(self):
        total = 0
        filtered_window = self.filter_window()
        for val in filtered_window:
            total += val.val
        avg = total / len(filtered_window)
        return avg

    def compute_rate(self):
        if not self.ready():
            return 0
        total = 0
        filtered_window = self.filter_window()
        for val in filtered_window:
            total += val.val
        start = self.window[0].ts
        end = self.window[-1].ts
        if end == start:
            return total
        return total / (end - start)


class EnergyTracker:
    def __init__(self,
                 sliding_window,
                 log_level=0):
        self.stime = time.time()
        self.sliding_window = sliding_window
        self.cc_tracker = EmissionsTracker(log_level=log_level)
        self.last_total_energy = 0

    def start(self):
        self.cc_tracker.start()

    def stop(self):
        self.cc_tracker.stop()
        emission_data = self.cc_tracker.final_emissions_data
        total_energy = emission_data.energy_consumed
        # convert kWh to Wh
        total_energy = total_energy * 1000
        total_energy -= self.last_total_energy
        # print(f"total_energy: {total_energy}")
        # print(f"last_total_energy: {self.last_total_energy}")
        self.last_total_energy = total_energy
        self.sliding_window.add_data(total_energy)
        return total_energy


class EnergyEstimator(SlidingWindow):

    def tracker(self):
        et = EnergyTracker(sliding_window=self, log_level=self.log_level)
        return et

    def print_state(self):
        print(f"EnergyEstimator: window size: {self.window_size}")
        print(f"EnergyEstimator: window: {self.window}")
        print(f"EnergyEstimator: avg consumption: {self.compute_avg()} Wh")
        print(f"EnergyEstimator: rate: {self.compute_rate()} Wh/sec")


class Timer:
    def __init__(self,
                 logger):
        self.stime = time.time()
        self.logger = logger

    def start(self):
        self.stime = time.time()

    def stop(self):
        total = time.time() - self.stime
        # print(f"time: {total}")
        self.logger.add_data(total)
        return total


class TimeEstimator(SlidingWindow):
    def start(self):
        return Timer(logger=self)
