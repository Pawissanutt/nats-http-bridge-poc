import math
from enum import Enum
from .estimator import EnergyEstimator, TimeEstimator


class RunMode(Enum):
    FAST = 1
    FAST_THEN_ACC = 2
    ACC = 3


class Battery:
    def __init__(self,
                 current=100,
                 total=100):
        self.current = current
        self.total = total

    def percentage(self):
        return self.current / self.total


class DecisionMaker:

    def decide(self, deadline=math.inf, log_param=False) -> RunMode:
        pass


class V1DecisionMaker(DecisionMaker):
    def __init__(self, battery: Battery, b_threshold=0.3):
        self.battery = battery
        self.b_threshold = b_threshold

    def check_battery(self, log_param=False):
        if log_param:
            print(f"battery percentage: {self.battery.percentage() * 100} %")
            print(f"battery threshold: {self.b_threshold * 100} %")
        return self.battery.percentage() > self.b_threshold

    def decide(self, deadline=math.inf, log_param=False) -> RunMode:
        if self.check_battery(log_param):
            return RunMode.FAST_THEN_ACC
        else:
            return RunMode.FAST


class V2DecisionMaker(V1DecisionMaker):
    def __init__(self,
                 battery: Battery,
                 energy_estimator: EnergyEstimator,
                 b_threshold=0.3,
                 er_threshold=0.1):
        super().__init__(battery,
                         b_threshold)
        self.er_threshold = er_threshold
        self.energy_estimator = energy_estimator

    def check_energy_consumption(self, log_param=False):
        e_rate = self.energy_estimator.compute_rate()
        if log_param:
            print(f"energy consumption rate: {e_rate} Wh/sec")
        return e_rate < self.er_threshold

    def decide(self, deadline=math.inf, log_param=False) -> RunMode:
        if not self.check_battery(log_param):
            return RunMode.FAST
        if self.check_energy_consumption(log_param):
            return RunMode.FAST_THEN_ACC
        else:
            return RunMode.FAST


class V3DecisionMaker(V2DecisionMaker):
    def __init__(self,
                 battery: Battery,
                 energy_estimator: EnergyEstimator,
                 f_time_estimator: TimeEstimator,
                 al_time_estimator: TimeEstimator,
                 ar_time_estimator: TimeEstimator,
                 b_threshold=0.3,
                 er_threshold=0.1,
                 upperbound_acc_to_fast_ratio=3):
        super().__init__(battery,
                         energy_estimator,
                         b_threshold,
                         er_threshold)
        self.f_time_estimator = f_time_estimator
        self.al_time_estimator = al_time_estimator
        self.ar_time_estimator = ar_time_estimator
        self.upper_acc_to_fast_ratio = upperbound_acc_to_fast_ratio

    def est_acc_time(self, log_param=False):
        if not self.f_time_estimator.ready():
            e_time = math.inf
        elif self.ar_time_estimator.ready():
            e_time = self.al_time_estimator.compute_avg() + self.ar_time_estimator.compute_avg()
            if log_param:
                print(f"estimate time on accuracy mode (local): {self.al_time_estimator.compute_avg()} sec")
                print(f"estimate time on accuracy mode (remote): {self.ar_time_estimator.compute_avg()} sec")
        else:
            e_time = self.f_time_estimator.compute_avg() * self.upper_acc_to_fast_ratio
        return e_time

    def check_est_time(self, deadline=math.inf, log_param=False):
        e_time = self.est_acc_time(log_param)
        if log_param:
            print(f"estimate time on accuracy mode (total): {e_time} sec")
            print(f"deadline: {deadline} sec")
        return e_time < deadline

    def decide(self, deadline=math.inf, log_param=False) -> RunMode:
        if not self.check_battery(log_param):
            return RunMode.FAST
        if self.check_est_time(deadline, log_param):
            return RunMode.ACC
        if self.check_energy_consumption(log_param):
            return RunMode.FAST_THEN_ACC
        else:
            return RunMode.FAST


class V4DecisionMaker(V3DecisionMaker):
    def decide(self, deadline=math.inf, log_param=False) -> RunMode:
        if not self.check_battery(log_param):
            return RunMode.FAST
        if not self.check_energy_consumption(log_param):
            return RunMode.FAST
        if self.check_est_time(deadline, log_param):
            return RunMode.ACC
        else:
            return RunMode.FAST_THEN_ACC
