== Prerequisite
* Docker
* Python 3
* pipenv `pip3 install pipenv`
* Install the dependencies via `pipenv install`


== Running
* Start NATS server, dummy server and consumer
[source,bash]
----
docker compose up -d
----

* Try to produce message to NATS server
[source,bash]
----
pipenv install
pipenv shell
python3 producer.py hello
----

What did it do?

. `producer.py` sends a message to NATS server.
. NATS server forwards a message to `consumer.py`.
. `consumer.py` make an HTTP request to the dummy HTTP server.
. `consumer.py` gets a response from the dummy HTTP server and reply it back to the NATS server.
. NATS server forward the reply back to `producer.py`.