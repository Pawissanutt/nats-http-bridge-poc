import json
import sys
import time

from flask import Flask, request, make_response
import logging

logging.basicConfig(level=logging.DEBUG)
app = Flask(__name__)


@app.route('/', methods=['POST'])
def handle():
    body = request.get_data()
    print(body)
    # time.sleep(0.5)
    return body


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=8080)
