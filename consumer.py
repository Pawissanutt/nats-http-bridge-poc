import asyncio
import logging
import os
from datetime import datetime
import time

import aiohttp
from nats import NATS
from nats.aio.msg import Msg

logging.basicConfig(level=logging.DEBUG)
CONCURRENCY_LEVEL = int(os.getenv('CONCURRENCY_LEVEL', '5'))
NATS_URL = os.getenv('NATS_URL', 'nats://127.0.0.1:4222')
TARGET_URL = os.getenv('TARGET_URL', 'http://localhost:8080')


async def request_server(req_body):
    start = time.time()
    async with aiohttp.ClientSession() as session:
        async with session.post(TARGET_URL, data=req_body) as response:
            body = await response.read()
            logging.info(f"[{datetime.now().isoformat()}] get data '{body}' "
                         f"in {time.time() - start} s")
            return body


endpoint = os.getenv('TARGET_URL', 'http://10.131.36.36:8501/v1/models/VGG16_Server:predict')
headers = {"content-type": "application/json"}


async def run_prediction(data):
    start = time.time()
    async with aiohttp.ClientSession() as session:
        async with session.post(endpoint, headers=headers, data=data) as response:
            body = await response.read()
            logging.info(f"[{datetime.now().isoformat()}] handle message "
                         f"in {time.time() - start} s")
            return body


async def main():
    nc = NATS()

    async def closed_cb():
        print("Connection to NATS is closed.")
        # await asyncio.sleep(0.1)
        asyncio.get_running_loop().stop()

    # It is very likely that the demo server will see traffic from clients other than yours.
    # To avoid this, start your own locally and modify the example to use it.
    options = {
        "servers": [NATS_URL],
        "closed_cb": closed_cb
    }

    await nc.connect(**options)
    print(f"Connected to NATS at {nc.connected_url.netloc}...")

    async def handle_message(msg: Msg):
        # body = await request_server(msg.data)
        body = await run_prediction(msg.data)
        await nc.publish(msg.reply, body)

    for i in range(CONCURRENCY_LEVEL):
        await nc.subscribe('help', 'worker', cb=handle_message)

    # try:
    #     async for msg in sub.messages:
    #         await handle_message(msg)
    # except Exception as e:
    #     pass

    # # Remove interest in subscription.
    # await sub.unsubscribe()
    #
    # # Terminate connection to NATS.
    # await nc.drain()


if __name__ == '__main__':
    # asyncio.run(main())
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main())
    loop.run_forever()
    loop.close()

    # loop.run_until_complete(request_server(b"test"))
