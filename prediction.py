import asyncio
import datetime
import os
import nats
import numpy as np
import json
import keras
from tensorflow.keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions

NATS_URL = os.getenv('NATS_URL', 'nats://localhost:4222')

model = keras.models.load_model('VGG16_Client.h5')


def pre_process(img):
    # load an image from file
    image = load_img(img, target_size=(224, 224))
    # convert the image pixels to a numpy array
    image = img_to_array(image)
    # reshape data for the model
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    # prepare the image for the VGG model
    image = preprocess_input(image)
    return (image)


async def request(msg):
    nc = await nats.connect(NATS_URL)
    rspn = None
    try:
        rspn = await nc.request("help", msg.encode(), timeout=30)
    except TimeoutError:
        print("Request timed out")
    await nc.drain()
    if rspn is not None:
        return rspn.data
    else:
        return None


if __name__ == '__main__':
    image = pre_process('car.jpg')
    yhat = model.predict(image)
    data = json.dumps({"instances": yhat.tolist()})
    result_bytes = asyncio.run(request(data))
    response = json.loads(result_bytes)
    yhat = response['predictions']
    # Converting back to array
    yhat = np.array(yhat)
    label = decode_predictions(yhat)
    print("================================================")
    print(label)
